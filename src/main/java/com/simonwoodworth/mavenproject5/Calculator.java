/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simonwoodworth.mavenproject5;

/**
 *
 * @author simon
 */
public class Calculator {
    
    public static int add (int a, int b) {
        return a + b;
    }
    
    public static int subtract (int a, int b) {
        return a - b;
    }
     
    public static int multiply (int a, int b) {
        return a * b;
    }
     
    public static int divide (int a, int b) {
        return a / b;
    }
    
    public static int square (int a) {
        return a*a;
    }
    
    public static int remainder (int a, int b) {
        return a%b;
    }
    
    public static int cube (int a) {
        return a*a*a;
    }
}
